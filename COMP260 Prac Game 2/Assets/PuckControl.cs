﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

	public AudioClip wallCollideClip;
	private AudioSource audio;
	public Transform startingPos;
	private Rigidbody rigidbody;
	public MovePaddle movePaddle;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
		rigidbody = GetComponent<Rigidbody> ();
		ResetPosition ();
	}
	public void ResetPosition() {
		//teleport to the starting position
		rigidbody.MovePosition(startingPos.position);
		//stop it from moving
		rigidbody.velocity = Vector3.zero;
		movePaddle.ResetAIPosition ();
	}

	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;

	void OnCollisionEnter(Collision collision){
		//check what we have hit
		if(paddleLayer.Contains(collision.gameObject)){
			//hit the paddle
		audio.PlayOneShot (wallCollideClip);
		Debug.Log ("Collision Enter: " + collision.gameObject.name);
	}
		else{
			//hit something else
			audio.PlayOneShot(wallCollideClip);
		}
	}

	void OnCollisionStay(Collision collision){
		Debug.Log ("Collision Stay: " + collision.gameObject.name);
	}

	void OnCollisionExit(Collision collision){
		Debug.Log ("Collision Exit: " + collision.gameObject.name);
	}

	// Update is called once per frame
	void Update () {
		
	}
}
