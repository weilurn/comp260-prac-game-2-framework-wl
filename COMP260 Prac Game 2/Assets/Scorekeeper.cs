﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class Scorekeeper : MonoBehaviour {


	static private Scorekeeper instance;

	static public Scorekeeper Instance {
		get{ return instance; }
	}

	//public int pointsPerGoal = 1;
	public int pointsPerGoal = 1;
	private int[] score = new int[2];
	public Text[] scoreText;
	public Text[] winMessage;

	public void ScoreGoal(int player){
		score [player] += pointsPerGoal;
		scoreText [player].text = score [player].ToString ();
		if (scoreText [0].text == "10") {
			winMessage [0].text = "Player 1 wins";
			Time.timeScale = 0;
		}
		if (scoreText [1].text == "10") {
			winMessage [0].text = "Player 2 wins";
			Time.timeScale = 0;
		}
	}
	//public void ScoreGoal(int player){
	//	score [player] += pointsPerGoal;
	//	scoreText [player].text = score [player].ToString ();
	//}
	


	// Use this for initialization
	void Start () {
		if (instance == null) {
			//save this instance
			instance = this;
		} else {
			//more than one exists
			Debug.LogError ("More than one Scorekeeper exists in the scene.");
		}
		//initialize singleton instance
		//....

		//reset the scores to zero
		for (int i = 0; i < score.Length; i++) {
			score [i] = 0;
			scoreText [i].text = "0";
		}

	}
	// Update is called once per frame
	void Update () {

	}
}
